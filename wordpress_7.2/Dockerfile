FROM alpine:3.9.6
ARG WITH_DEBUG=0

RUN apk add --update --no-cache \
        php7 \
        php7-amqp \
        php7-apcu \
        php7-bcmath \
        php7-calendar \
        php7-common \
        php7-ctype \
        php7-curl \
        php7-dom \
        php7-exif \
        php7-fileinfo \
        php7-fpm \
        php7-ftp \
        php7-gd \
        php7-iconv \
        php7-imagick \
        php7-json \
        php7-mbstring \
        php7-opcache \
        php7-openssl \
        php7-mysqlnd \
        php7-mysqli \
        php7-phar \
        php7-posix \
        php7-redis \
        php7-session \
        php7-simplexml \
        php7-pcntl \
        php7-soap \
        php7-sockets \
        php7-tokenizer \
        php7-xml \
        php7-xmlwriter \
        php7-xmlreader \
        php7-zip \
        php7-zlib \
        nginx curl git openssh-client busybox htop && \
    ln -s /usr/sbin/php-fpm7 /usr/sbin/php-fpm && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

## Install and Enable xdebug
RUN if [ $WITH_DEBUG -eq "1" ]; then \
        apk add --update --no-cache php7-xdebug && \
        echo 'zend_extension=xdebug.so' > /etc/php7/conf.d/xdebug.ini; \
    fi

COPY --from=ochinchina/supervisord:latest /usr/local/bin/supervisord /usr/local/bin/supervisord

ENV APP_DIR /app
ENV APP_USER app
ENV APP_USER_ID 1001
ENV APP_GROUP_ID 1001

COPY config /var/config
RUN rm -rf /etc/nginx/conf.d/* && \
    sed -i "s/user nginx/user ${APP_USER}/" /etc/nginx/nginx.conf && \
    rm -rf /etc/supervisord.conf && \
    cp -rf /var/config/supervisor/. /etc/supervisor && \
    cat /var/config/php/php-fpm.conf > /etc/php7/php-fpm.conf && \
    mkdir -p /var/log/php-fpm && \
    mkdir -p /var/log/nginx && \
    mkdir -p /run/nginx && \
    mkdir -p /var/tmp/nginx && \
    chmod -R 700 /var/tmp/nginx && \
    ## Cleanup
    rm -rf /var/cache/apk/* && \
    rm -rf /tmp/* && \
    rm -rf /var/config

COPY entry_point.sh /
RUN chmod u+x /entry_point.sh

ENTRYPOINT ["/entry_point.sh"]