#!/bin/sh

apk --no-cache add shadow

groupadd -g $APP_GROUP_ID $APP_USER
useradd -u $APP_USER_ID -g $APP_GROUP_ID -s /bin/sh -m $APP_USER

apk --no-cache del shadow
echo "$APP_USER:123" | chpasswd

if [ ! -d $APP_DIR ]; then
  mkdir -p $APP_DIR && \
  chown -R $APP_USER:$APP_USER $APP_DIR;
fi

chown -R $APP_USER:$APP_USER /var/log/php-fpm
chown -R $APP_USER:$APP_USER /var/log/nginx
chown -R $APP_USER:$APP_USER /var/tmp/nginx

exec supervisord -c /etc/supervisor/supervisord.conf
